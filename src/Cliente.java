import javax.swing.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

public class Cliente {

    Random rnd = new Random();
    Scanner scan = new Scanner(System.in);

    //<editor-fold desc="Variables globales">
    private int codigo;
    private String nombre;
    private long sueldo;
//    private int repetir = 0;
    //</editor-fold>

    //<editor-fold desc="Constructores">
    public Cliente() {
    }

    public Cliente(int codigo, String nombre, long sueldo) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.sueldo = sueldo;
    }
    //</editor-fold>

    //<editor-fold desc="Getters & Setters">
    public int getCodigo() {

        return codigo;
    }

    public void setCodigo() {

        int code = rnd.nextInt(10000) + 1;

        this.codigo = code;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre() {
        int rellenoEspacios, i;


        System.out.println("Nombre: ");
        nombre = scan.nextLine();
        if (nombre.length() < 50) {
            rellenoEspacios = 50 - nombre.length();
            for (i = 0; i < rellenoEspacios; i++) {
                nombre = nombre + " ";
            }
        }
    }

    public long getSueldo() {
        return sueldo;
    }

    public void setSueldo() {
        Scanner scan = new Scanner(System.in);
        long sueldo;
        String numCadena;

        do {
            System.out.println("Saldo: ");
            sueldo = scan.nextLong();
            numCadena = Long.toString(sueldo);

            if (numCadena.length() != 10)
                JOptionPane.showMessageDialog(null, "El saldo tiene,que tener 10 digitos",
                        "Error", JOptionPane.ERROR_MESSAGE);

        } while (numCadena.length() != 10);

        this.sueldo = sueldo;
    }
    //</editor-fold>

    //<editor-fold desc="Funciones">
//    public void compruebaCodigoCliente(String ficheroCliente, String codigoCliente) {

//        String nom="";

//        int mida;

//        int pos=5;

//        Scanner entrada = null;
//        String linea;
//        String[] camposLinea = new String[0];
//        String caracterSeparador = "#";
//
//        try {
//            //creamos un objeto File asociado al fichero seleccionado
//            File f = new File(ficheroCliente);
//            //creamos un Scanner para leer el fichero
//            entrada = new Scanner(f);
//            //mientras no se llegue al final del fichero
//            while (entrada.hasNext()) {
//                linea = entrada.nextLine();  //se lee una línea
//
//                //cortamos la linea por donde encuentre un "#" y el contenido lo ponemos en un array
//                camposLinea = linea.split(caracterSeparador);
//                /* comprobamos que el primer campo de array sea igual al "codCliente"
//                Si existe, guardamos esa línea
//                */
//                if (camposLinea[0].equals(codCliente) && repetir == 0) {
//
//                    System.out.println("\tDatos del cliente\n" +
//                            "\t-----------------\n" +
//                            "Cod.Cliente: " + camposLinea[0] + "\n" +
//                            "Nombre: " + camposLinea[1] + "\n" +
//                            "Saldo: " + camposLinea[2]);
//
//                    repetir++;//
//                    return camposLinea[0];
//                }
//            }
//        } catch (
//                FileNotFoundException e) {
//            return String.valueOf((Object) null);
//
//        } finally {
//            if (entrada != null) {
//                entrada.close();
//            }
//        }
//        if (repetir < 1) return camposLinea[0]; // devuelve el campo con el Código del cliente
//        else return camposLinea[0] = codCliente;

//    }

    public void guardaDatosClienteEnFicheroDeTexto(String archivo, int codi, String nom, long saldo) throws IOException {

        int numeroRegistros;
        //el tamano de cada registro de cliente tiene que tener 80 bytes

        //Si el archivo no exite y no es un archivo. Se crea el archivo automaticamente
//        if (archivo.exists() && !archivo.isFile()) {
//            throw new IOException(archivo.getName() + " no es un archivo");
//        }
        try {
            RandomAccessFile escribeEnArchivo = new RandomAccessFile(archivo, "rw");
            /*obtenemos la longitud de bytes del archivo / entre el tamano del registro, sabemos cuantos registros hay
            ejemplo: 'escribeArchivo.lenght()' es de 180 bytes y 'tamanoRegistro' ocupa 80 bytes entonce sabremos
            que tenemos dos registros. 'escribeArchivo.lengh() / (double) tamanoRegistro)' = 2 registros.

            Hacemos un casting a 'Int' y con el metodo 'ceil' de la clase 'Math' aprosimamos el resultado al siguiente numero
            superior. Asi, nos aseguramos que el numero de registro es siempre correcto.

             */
//            numeroRegistros = (int) Math.ceil((double)escribeEnArchivo.length() / (double) tamanoRegistro);

            escribeEnArchivo.writeInt(codi);
            escribeEnArchivo.writeChars(nom);
            escribeEnArchivo.writeLong(saldo);
            escribeEnArchivo.close();

        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {

        }

        JOptionPane.showMessageDialog(null, "Cliente introducido con exito.");


    }

    public void compruebaCliente(String ficheroCliente) {

        int longitugRegistro = 80;
        int posicion = 5;

        try {

            RandomAccessFile firaf = new RandomAccessFile(ficheroCliente, "rw");

            longitugRegistro = Integer.BYTES + Character.BYTES * 50 + Long.BYTES;

            firaf.seek(longitugRegistro * (posicion - 1));

            codigo = firaf.readInt();

            for (int i = 1; i < 50; i++) {

                nombre = nombre + firaf.readChar();

            }

            sueldo = firaf.readLong();

            System.out.println();

            firaf.close();


        } catch (FileNotFoundException ex) {


        } catch (IOException ex) {


        }
//
//        Scanner entrada = null;
//        String linea;
//
//        String caracterSeparador = "#";
//        String[] camposLinea = new String[0];
//        int codCliente;
//
//        do {
//            //Introducimos el codCliente a buscar
//            System.out.print("Introduce Codigo de cliente: ");
//            codCliente = scan.nextInt();
//
//            try {
//                //creamos un objeto File asociado al fichero seleccionado
//                File f = new File(fichero);
//                //creamos un Scanner para leer el fichero
//                entrada = new Scanner(f);
//
//                //mientras no se llegue al final del fichero
//                while (entrada.hasNext()) {
//                    linea = entrada.nextLine();  //se lee una línea
//
//                    //cortamos la linea por donde encuentre un "#" y el contenido lo ponemos en un array
//                    camposLinea = linea.split(caracterSeparador);
//
//                    /* comprobamos que el primer campo de array sea igual al "codCliente"
//                    Si existe, guardamos esa línea
//                    */
//                    if (Objects.equals(camposLinea[0], codCliente)) {  //&& repetir == 0
//
//                        System.out.println("\tDatos del cliente\n" +
//                                "\t-----------------\n" +
//                                "Cod.Cliente: " + camposLinea[0] + "\n" +
//                                "Nombre: " + camposLinea[1] + "\n" +
//                                "Saldo: " + camposLinea[2]);
//                        break;
//                    }
//                }
//            } catch (FileNotFoundException e) {
//                return String.valueOf(entrada);
//            } finally {
//                if (entrada != null) {
//                    entrada.close();
//                }
//            }
//            if (!Objects.equals(codCliente, camposLinea[0])) {
//
//                JOptionPane.showMessageDialog(null, "El codigo de cliente " + codCliente + " NO ExISTE",
//                        "Error", JOptionPane.ERROR_MESSAGE);
//
//                System.out.println();
//
//            }
//
//        } while (!Objects.equals(codCliente, camposLinea[0]));
//
//        return camposLinea[0];
//    }

//    public void modificaDatosClienteEnFicheroTexto(String fichero, String ficheroAux, String datosCliente, int codCliente) {
//        File ficheroCliente = new File(fichero);
//        File ficheroAuxiliar = new File(ficheroAux);
//
//        try {
//            //si existe el fichero inicial
//            if (ficheroCliente.exists()) {
//                //abro un flujo de lectura*/
//                BufferedReader Flee = new BufferedReader(new FileReader(ficheroCliente));
//                String Slinea;
//                //recorro el fichero de texto linea a linea
//                while ((Slinea = Flee.readLine()) != null) {
//                    //si la linea obtenida es igual al la buscada
//                    if (Slinea.toUpperCase().trim().contains(codCliente.toUpperCase().trim())) {
//                        //escribo la nueva linea en vez de la que tenia
//                        EscribirFichero(ficheroAuxiliar, datosCliente);
//                    } else {
//                        //escribo la linea antigua
//                        EscribirFichero(ficheroAuxiliar, Slinea);
//                    }
//                }
//                //obtengo el nombre del fichero anterior
//                String ficheroAnterior = ficheroCliente.getName();
//                //borro el fichero anterior
//                Flee.close();
//                BorrarFichero(ficheroCliente);
//                //renombro el nuevo fichero con el nombre del fichero anterior
//                ficheroAuxiliar.renameTo(new File(ficheroAnterior));
//                //cierro el flujo de lectura
//                Flee.close();
//            } else {
//                System.out.println("Fichero No Existe");
//            }
//        } catch (Exception ex) {
//            //captura un posible error y le imprime en pantalla
//            System.out.println(ex.getMessage());
//        }
//    }

//    public static void EscribirFichero(File nuevoFichero, String nuevaLinea) {
//
//        try {
//            //Si no Existe el fichero lo crea
//            if (!nuevoFichero.exists()) {
//                nuevoFichero.createNewFile();
//            }
//            /*abre un Flujo de escritura,sobre ficheroEscribe con codificacion utf-8.
//             *true es por si existe el fichero seguir añadiendo texto y no borrar lo que tenia*/
//            BufferedWriter ficheroEscribe = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(nuevoFichero, true), StandardCharsets.UTF_8));
//            //escribe en el fichero la cadena que recibe la función.
//            ficheroEscribe.write(nuevaLinea + "\n");
//            //cierra el flujo de escritura
//            ficheroEscribe.close();
//        } catch (Exception ex) {
//            //captura un posible error y lo imprime en consola
//            System.out.println(ex.getMessage());
//        }
//    }

//    public static void BorrarFichero(File ficheroCliente) {
//        try {
//            //si existe el fichero
//            if (ficheroCliente.exists()) {
//                //borra el fichero
//                ficheroCliente.delete();
//                JOptionPane.showMessageDialog(null, "Cliente modificado con exito.", "Info", JOptionPane.INFORMATION_MESSAGE);
//            }
//        } catch (Exception ex) {
//            //captura un posible error y le imprime en cosonla
//            System.out.println(ex.getMessage());
//        }
//    }
        //</editor-fold>
    }
}
